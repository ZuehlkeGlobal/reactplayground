// we always need to import react, since the jsx code in this file is transpiled to something like "React.component('span',...)"
import React from 'react';
import PropTypes from 'prop-types';

const Coordinates = ({ coordinates }) => {
  if (!coordinates || !coordinates.x || !coordinates.y) {
    // stateless functional components cannot return null, instead we render an empty span
    return <span />;
  }

  if (!coordinates.type || coordinates.type !== 'WGS84') {
    return <span>Unknown coordinates type!</span>;
  }

  return (
    <span>
      {coordinates.x} lat / {coordinates.y} lng
    </span>
  );
};

Coordinates.propTypes = {
  coordinates: PropTypes.object.isRequired
};

export default Coordinates;
