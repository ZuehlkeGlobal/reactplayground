import log from 'loglevel';
import React from 'react';
import { render } from 'react-dom';

import MainView from './views/MainView';

import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/font-awesome/css/font-awesome.min.css';

import './assets/reactPlayground.styl';

log.setLevel('debug');

/**
 * This is our top-level React component
 * here, we can do some bootstrapping, stuff like routing etc...
 */
function App() {
  // for the moment, we always render our main view
  return <MainView />;
}

// render the react app and insert it into the DOM tree
render(<App />, document.getElementById('app-root'));
